# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://nomnomnom2@bitbucket.org/nomnomnom2/stroboskop.git


Naloga 6.2.3:
https://bitbucket.org/nomnomnom2/stroboskop/commits/08fe92da86fafa9f4fd2105623df1af94a300559

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/nomnomnom2/stroboskop/commits/d095404980be24d30d998d052aa749fa94014e26

Naloga 6.3.2:
https://bitbucket.org/nomnomnom2/stroboskop/commits/699aaed51c08a31ae07656b48d1fddeacbc67b95

Naloga 6.3.3:
https://bitbucket.org/nomnomnom2/stroboskop/commits/e3a28f90b0bdc9293804766ed3aee4e1f981b4cb

Naloga 6.3.4:
https://bitbucket.org/nomnomnom2/stroboskop/commits/c7d2dac37fc1b7e5d80e25d2901910839b4d015a

Naloga 6.3.5:

git checkout -b izgled
git commit -m
git push 
git checkout master
git merge izgled


## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/nomnomnom2/stroboskop/commits/0f03b4246ef010415325ecc97312c20e28462284

Naloga 6.4.2:
https://bitbucket.org/nomnomnom2/stroboskop/commits/5ef8f96c0cf00b69370841f3763a7c97f66559d1

Naloga 6.4.3:
https://bitbucket.org/nomnomnom2/stroboskop/commits/3496148c02515dd7fc4768d0c1018e29bfd06dcf

Naloga 6.4.4:
https://bitbucket.org/nomnomnom2/stroboskop/commits/138f0343a206df9346b7f92e74ecefe07acf3cbb